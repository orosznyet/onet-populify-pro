<div class="wrap">
	<h2 class="onet-admin-header">
		<?php _e("Populify Pro", "onetpop"); ?> <small><?php echo sprintf(__("made by %s","onetpop"),"József Koller"); ?></small>
	</h2>

	<div id="message" class="updated below-h2" style="display:<?php echo $msg == "" ? "none" : "block"; ?>"><p><?php echo $msg; ?></p></div>

	<!-- Secondary coll -->
	<div class="postbox-container" style="width:300px;position:absolute;right:20px;">
		<div class="metabox-holder">	
			<div class="meta-box-sortables">

				<!-- support -->
				<div id="<?php echo ONETPOP_OPT_PREFIX; ?>-troubleshoot" class="postbox">
					<div class="handlediv"><br /></div>
					<h3 class="hndle"><span><?php _e("Contact and Support", "onetpop"); ?></span></h3>
					<div class="inside">
						<?php _e('Thank you for using this plugin, we hope you like ONet Populify Pro for Wordpress. If you have any question, problem or suggestion feel free to create a thread on official Wordpress Plugin forum. You can access quick help for almost each options if you move your mouse over the "?" icons. Also you can find "Read me" if you click on "Help" button above this box.', "onetpop"); ?>
						<br/><br/>
						<strong><?php _e("Premium support","onetpop"); ?></strong><br/>
						<?php echo sprintf(__("The developer provides premium support for low hourly rate. If you want to get exclusive support directly from the developer please write to %s.","onetpop"),'<a href="mailto:orosznyet@gmail.com" target="_blank">orosznyet@gmail.com</a>'); ?>
					</div>
				</div><!-- // .support -->

			</div>
		</div>
	</div>

	<!-- Main col -->
	<div class="postbox-container" style="float:none;width:auto;margin-right: 330px;">
		<div class="metabox-holder">	
			<div class="meta-box-sortables">
				<form method="post">
					<?php wp_nonce_field($opts->custom_id, ONETPOP_OPT_PREFIX.'nonce', false ); ?>
					
					<div style="clear:both"></div>
					<div id="oPPP-settings" class="onet-admin-panel">
						<h1 class="category cat-general">
							<abbr title="<?php _e("The more options you use the most relevant list you get.","onetpop"); ?>"><?php _e("General settings","onetpop"); ?></abbr>
						</h1>
						<div class="content cont-general">
							<!-- Global switcher -->
							<div class="row">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>global"><?php _e("Activate counter","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo ONETPOP_OPT_PREFIX; ?>global" id="<?php echo ONETPOP_OPT_PREFIX; ?>global" class="parse-onet-onof" <?php echo $opts->global ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("You can turn off the whole Populify Pro score system with this.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

							<!-- Filter double counter -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>double_count_filter"><?php _e("Prevent false counts","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" id="<?php echo ONETPOP_OPT_PREFIX; ?>double_count_filter" name="<?php echo ONETPOP_OPT_PREFIX; ?>double_count_filter" class="parse-onet-onof" <?php echo $opts->double_count_filter ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Recommended for small websites in order to make calculations accurate.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

							<!-- Longread score -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>longread_count"><?php _e("Longread score","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo ONETPOP_OPT_PREFIX; ?>longread_count" id="<?php echo ONETPOP_OPT_PREFIX; ?>longread_count" class="parse-onet-onof" <?php echo $opts->modules->longread_count ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Score additional points if user reads a post for user-defined time.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

							<!-- Longread treshold -->
							<div class="row hideIfNoGlobal hideIfNoLongread">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>longread_mins"><?php _e("Longread treshold (in minutes)","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="number" name="<?php echo ONETPOP_OPT_PREFIX; ?>longread_mins" id="<?php echo ONETPOP_OPT_PREFIX; ?>longread_mins" class="onet-input" value="<?php echo $opts->longread_mins; ?>" />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Score additional points if user reads a post for long time.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

							<!-- Comments score -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>coms"><?php _e("Comments score","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo ONETPOP_OPT_PREFIX; ?>coms" id="<?php echo ONETPOP_OPT_PREFIX; ?>coms" <?php echo $opts->modules->coms ? "checked='checked'" : ""; ?> class="parse-onet-onof" />
								</div>
								<div style="clear:both"></div>
							</div>

							<?php
							foreach ($shareSettings AS $drop) {
							?>
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX.$drop['var']; ?>"><?php echo $drop['name']; ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo ONETPOP_OPT_PREFIX.$drop['var']; ?>" id="<?php echo ONETPOP_OPT_PREFIX.$drop['var']; ?>" class="parse-onet-onof" <?php echo $opts->modules->$drop['var'] ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php echo $drop['hint']; ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>
							<?php
							}
							?>
						</div>

						<h1 class="category cat-prevention hideIfNoGlobal hideIfNoPrevention">
							<abbr title="<?php _e("Important: Change this setting only if you noticed something strange about the plugin."); ?>"><?php _e("Prevention settings","onetpop");?></abbr>
						</h1>
						<div class="content cont-prevention hideIfNoGlobal hideIfNoPrevention">

							<!-- Prevention methos -->
							<div class="row" style="padding-bottom:5px">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>prevent_method" class="onet-radioName"><?php _e("Default method","onetpop"); ?></label>
								</div>
								<div class="right">
									<label class="onet-radioWrap">
										<input type="radio" name="<?php echo ONETPOP_OPT_PREFIX; ?>prevent_method" value="cookie" <?php echo $opts->prevent_method != "db" ? "checked='checked'" : ""; ?>>
											<?php _e("Client-side cookie.","onetpop"); ?> <strong><?php _e('Recommended','onetpop'); ?></strong>
										
									</label>
									<span class="onet-help" style="top:1px">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Optimal for bigger sites but can be evaded easily on client side.","onetpop"); ?></span>
									</span>
									<div style="height:11px;font-size:0px;line-height:0px;"></div>
									<label class="onet-radioWrap">
										<input type="radio" name="<?php echo ONETPOP_OPT_PREFIX; ?>prevent_method" value="db" <?php echo $opts->prevent_method == "db" ? "checked='checked'" : ""; ?>>
										<?php _e("Server-side database (IP restriction).","onetpop"); ?>
									</label>
									<span class="onet-help" style="top:1px">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Consumes more resources on server side and hard to evade on client side.","onetpop"); ?></span>
									</span>

								</div>
								<div style="clear:both"></div>
							</div>

							<!--  Prevention timeout -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>prevent_db_lifetime"><?php _e("Prevention timeout (in days)","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="number" name="<?php echo ONETPOP_OPT_PREFIX; ?>prevent_db_lifetime" id="<?php echo ONETPOP_OPT_PREFIX; ?>prevent_db_lifetime" class="onet-input" value="<?php echo $opts->prevent_db_lifetime; ?>" />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e('Recommended value : 1-14. Use zero to store prevention entries as long as possible. Using zero value can significantly increase database size.',"onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

						</div>

						<h1 class="category cat-db hideIfNoGlobal">
							<?php _e("Storage settings","onetpop"); ?>
						</h1>
						<div class="content cont-db hideIfNoGlobal">
							
							<!--  Tracked post max age -->
							<div class="row">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>max_content_age"><?php _e("Track posts not older than provided days","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="number" name="<?php echo ONETPOP_OPT_PREFIX; ?>max_content_age" id="<?php echo ONETPOP_OPT_PREFIX; ?>max_content_age" class="onet-input" value="<?php echo $opts->max_content_age; ?>">
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Recommended value: ~30 (one month). If a post older than provided days it will not be tracked in database. Use zero in order to track every post.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

							<!--  Tracked post max age -->
							<div class="row">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>db_lifetime"><?php _e("Store collected datas for provided days","onetpop"); ?></label>
								</div>
								<div class="right">
									<input type="number" name="<?php echo ONETPOP_OPT_PREFIX; ?>db_lifetime" id="<?php echo ONETPOP_OPT_PREFIX; ?>db_lifetime" class="onet-input" value="<?php echo $opts->db_lifetime; ?>">
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Recommended value: ~30 (one month). Higher value means more detailed statitcs but use more database as well. Type zero to store all collected datas.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>

							<!--  Truncatle database -->
							<div class="row">
								<div class="left">
									<label for="<?php echo ONETPOP_OPT_PREFIX; ?>truncate"><?php _e("Empty tracking database","onetpop"); ?> (<?php echo $tableSize; ?>)</label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo ONETPOP_OPT_PREFIX; ?>truncate" id="<?php echo ONETPOP_OPT_PREFIX; ?>truncate" class="parse-onet-onof onet-yesno"/>
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Remove all collected datas to free up database space and increase performance.","onetpop"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div>
						</div>

					</div>

					<input type="submit" class='button-primary' name="update_onetpop" value="<?php _e('Save Changes','onetpop'); ?>" style="margin: 30px 10px;" />
					<div style='clear:both;'></div>
				</form>
			</div>
		</div>
	</div>
	
	<div style="clear:both;"></div>
	
	
</div>