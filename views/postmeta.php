<div class="misc-pub-section">
	<?php _e("Current index",'onetpop'); ?>: <strong><?php echo intval($idxs['index']); ?></strong>
</div>
<?php if ($opts->modules->fb) : ?>
	<div class="misc-pub-section">
		<?php _e("Facebook points (overall)",'onetpop') ?>: <strong><?php echo $soc['fb']; ?></strong>
	</div>
<?php endif; if ($opts->modules->tw) : ?>
	<div class="misc-pub-section">
		<?php _e("Twitter points (overall)",'onetpop'); ?>: <strong><?php echo $soc['tw']; ?></strong>
	</div>
<?php endif; if ($opts->modules->gp) : ?>
	<div class="misc-pub-section">
		<?php _e("Facebook points (overall)",'onetpop'); ?>: <strong><?php echo $soc['gp']; ?></strong>
	</div>
<?php endif; if ($opts->modules->pint) : ?>
	<div class="misc-pub-section">
		<?php _e("Pinterest points (overall)",'onetpop'); ?>: <strong><?php echo $soc['pint']; ?></strong>
	</div>
<?php endif; if ($opts->modules->longread_count) : ?>
	<div class="misc-pub-section">
		<?php _e("Longread points",'onetpop'); ?>: <strong><?php echo intval($idxs['longread']); ?></strong>
	</div>
<?php endif; if ($opts->modules->coms) : ?>
	<div class="misc-pub-section">
		<?php _e("Comments",'onetpop'); ?>: <strong><?php echo intval($coms); ?></strong>
	</div>
<?php endif; ?>

<label for="<?php echo ONETPOP_OPT_PREFIX; ?>dismiss_counter" class="misc-pub-section">
	<input type="checkbox" id="<?php echo ONETPOP_OPT_PREFIX; ?>dismiss_counter" name="<?php echo ONETPOP_OPT_PREFIX; ?>dismiss_counter" <?php echo $isDismissed ? 'checked="checked"' : ""; ?>/> <?php _e('Exclude from popularity counter','onetpop'); ?>
</label>