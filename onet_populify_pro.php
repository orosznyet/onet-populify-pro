<?php 
/*
===================================================================
Plugin Name:  ONet Populify Pro
Plugin URI:   http://onetdev.com/repo/onet-populify-pro
Description:  Get social value for your posts based on Twitter, Facebook, Pinterest, Google+ share counters, average reading time and comment number. Usefull for advanced users.
Git:          https://bitbucket.org/orosznyet/onet-populify-pro
Version:      1.0
Author:       József Koller
Author URI:   http://www.onetdev.com
Text Domain:  onetpop
Domain Path:  /lang

===================================================================

Copyright (C) 2013 József Koller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

===================================================================

Graph API for likes/comment/share: http://api.facebook.com/restserver.php?method=links.getStats&urls=http://onetdev.com/
Twitter API for share number: http://urls.api.twitter.com/1/urls/count.json?url=http://onetdev.com

===================================================================
*/




/*+++++++++++++++++++++++++++++++++++++++++++
Default contants
+++++++++++++++++++++++++++++++++++++++++++*/




global $wpdb,$wp;
define("ONETPOP_OPT_PREFIX", "onetpop_");                                  // option prefix, widget cache prefix
define("ONETPOP_DB_VIEW", $wpdb->prefix.ONETPOP_OPT_PREFIX."view");        // Post view and comment cache by days
define("ONETPOP_DB_PREVENT", $wpdb->prefix.ONETPOP_OPT_PREFIX."prevent");  // Prevention database
define("ONETPOP_OPTIONS_FIELD", ONETPOP_OPT_PREFIX."settings");                       // Settings field in options table
define("ONETPOP_PREVENT_COOKIE",ONETPOP_OPT_PREFIX."pc_");                 // Cookie name for prevention
define("ONETPOP_ASSESTS",dirname(__FILE__)."/assets");                     // Assets folder
define("ONETPOP_VIEWS",dirname(__FILE__)."/views");                        // Views folder
define("ONETPOP_ASSETS_URI", site_url("/wp-content/plugins/onet-populify-pro/assets") ); // Assets URI



/*+++++++++++++++++++++++++++++++++++++++++++
Register init function and installation hooks
+++++++++++++++++++++++++++++++++++++++++++*/




// Global binding for instance wrap
add_action( 'init', 'ONetPopulifyPro_wrap' );
function ONetPopulifyPro_wrap() {
global $ONetPopulifyPro_inst;
	$ONetPopulifyPro_inst = new ONetPopulifyPro();
}


// Hook for installer
register_activation_hook(__FILE__, "ONetPopulifyPro_install");
function ONetPopulifyPro_install () {
global $wp,$wpdb;

	if ( version_compare("5.0", phpversion(), '>') ) {
		echo '<strong>'.__('Plugin installation failed.','onetpop').'</strong> '.
			sprintf(__('Sorry, ONet Populify Pro requires at least PHP 5.0 but your version is PHP %s.','onetpop'),phpversion());
		exit;
	}

	/*
	Database description
	rid - record id
	ID - post ID
	view - counted views
	comment - number or comment that day
	fb - shares on facebook
	tw - share on twitter
	soc_last_hcekc - last check of social shares (decrease server load, helps cache)
	longred - points for long read
	point_index - calculated index based on other points
	date - special date field (YYYYmmdd)
	*/
	$sql = "CREATE TABLE IF NOT EXISTS `".ONETPOP_DB_VIEW."` (
		`rid` bigint(20) NOT NULL AUTO_INCREMENT,
		`ID` bigint(20) NOT NULL,
		`view` int(10) NOT NULL,
		`comment` int(5) NOT NULL,
		`fb` int(10) NOT NULL,
		`tw` int(10) NOT NULL,
		`gp` int(10) NOT NULL,
		`pint` int(10) NOT NULL,
		`soc_last_check` int(10) NOT NULL,
		`longread` int(10) NOT NULL,
		`point_index` int(10) NOT NULL,
		`date` int(10) NOT NULL,
		UNIQUE KEY `rid` (`rid`)
		);";
	if (!$wpdb->query($sql)) { echo $wpdb->print_error(); exit; }

	$sql ="CREATE TABLE IF NOT EXISTS `".ONETPOP_DB_PREVENT."` (
		`PID` bigint(20) NOT NULL AUTO_INCREMENT,
		`type` varchar(10) NOT NULL,
		`obj_id` bigint(20) NOT NULL,
		`time` int(10) NOT NULL,
		`ip` varchar(45) NOT NULL,
		PRIMARY KEY (`PID`)
		);";
	if (!$wpdb->query($sql)) { echo $wpdb->print_error(); exit; }

	delete_option( ONETPOP_OPTIONS_FIELD ); // It's really importent to remove previous settings on fresh install
	add_option(ONETPOP_OPTIONS_FIELD, array( "custom_id" => md5( NONCE_SALT.(rand(100,999)*rand(200,500)))  ), "", true ); // Then push the new values of course.
}

// Uninstall :((
register_uninstall_hook(__FILE__, "ONetPopulifyPro_uninstall");
function ONetPopulifyPro_uninstall () {
global $wp,$wpdb;
	# $wpdb->query("DROP TABLE `".ONETPOP_DB_VIEW."`,`".ONETPOP_DB_PREVENT."`"); // This would drop the tables but I don't want to do that :(
	$wpdb->query("TRUNCATE TABLE `".ONETPOP_DB_VIEW."`,`".ONETPOP_DB_PREVENT."`"); // Truncate tables to keep the place clean.
	$wpdb->query("UPDATE `".$wpdb->prefix."options` SET autoload='no' WHERE option_key='".ONETPOP_OPTIONS_FIELD."'"); // Keep settings but turn off autoload.
}




/*+++++++++++++++++++++++++++++++++++++++++++
This the ultra mega hyper super stuff
+++++++++++++++++++++++++++++++++++++++++++*/




class ONetPopulifyPro {
	var $menu_item = null;      // menu item hook
	var $cap = null;            // required cap for plugin settings page
	var $opts = array();        // options array
	var $plugin = null;         // plugin ID



	/******************************************
	* CONSTRUCTING & REGISTERING              *
	******************************************/



	/**
	* Register all important functions and filters
	* @since 1.0
	* @param void
	* @return void
	**/
	function __construct () {
		// Update some major inner variable
		$this->plugin = plugin_basename( __FILE__ );
		$this->cap = apply_filters( 'onetrt_cap', 'manage_options' );

		// Load language lib
		load_plugin_textdomain('onetpop', false, dirname( $this->plugin ) . '/lang/' );

		// Fire up settings
		$this->opts = $this->get_opts();

		// Check JSON lib. In case of miracle load internal JSON handler functions.
		if (!function_exists("json_encode")) { require_once(dirname(__FILE__)."/lib/JSON.php"); }

		// Add actions
		add_action('admin_menu',                     array(&$this,'admin_menu') );           // Add link to settings
		add_action('widgets_init',                   create_function('', 'return register_widget("ONetPopulifyPro_Widget");') );  // Add available widget
		add_action('add_meta_boxes',                 array(&$this,'postmeta_wrap') );        // Add the related metabox for the admin
		add_action('admin_enqueue_scripts',          array(&$this,'admin_scripts') );        // Register administration related stuff.
		add_action('save_post',                      array(&$this,'postmeta_save') );        // Save post related changes.
		add_action('wp_footer',                      array(&$this,'counter_client') );       // Counter script on client side
		add_action('wp_footer',                      array(&$this,'counter_post_process') );
		add_action('parse_request',                  array(&$this,'counter_longread') );     // "Longread" AJAX handler.

		// Add data/content filters
		add_filter('plugin_action_links_'.$this->plugin, array(&$this,'plugin_settings_link'), 10, 2 );
		add_filter('contextual_help',                array(&$this,'admin_help'), 10, 3);
	}

	/**
	* This function makes default settings available
	* @since 1.0
	* @param void
	* @return (array) default variables
	**/
	static function get_default_opts () {
		return array(
			"custom_id" =>          md5( NONCE_SALT.(rand(100,999)*rand(200,500)) ), // A simple random string, no security reason
			"global" =>             true,         // If the whole plugin is global or not, simple as hell.
			"modules" => array(                   // These are the available scoring modules.
				"longread_count" => true,         // Count long reads
				"coms" =>           true,         // Score by comment number
				"fb" =>             true,         // Facebook
				"tw" =>             true,         // Twitter
				"gp" =>             true,         // The Google's Plus
				"pint" =>           true          // Pinterest 
			),
			"double_count_filter" => true,        // Longread count can be tricked so with this on you can prevent most of the nasty cheates
			"last_autoclean" =>     0,            // UNIX timestamp for last autoclean, initial value.
			"longread_mins" =>      3,            // Minutes before longread gets counted
			"max_content_age" =>    28,           // Track maximum n days old articles. For old articles it might be unnecessary to track everything.
			"db_lifetime" =>        28,           // Maximum lifetime for stored tracking results.
			"prevent_db_lifetime" => 3,           // Maximum number of days until prevention meta is stored in the database
			"prevent_method" => "cookie"          // Pevention method, cookie or DB
		);
	}

	/**
	* Get plugin related main options
	* @since 1.0
	* @param [(boolean) returning value type is assoc or object]
	* @param [(boolean) refresh $this->opts or not]
	* @return (array|object) up to date options
	**/
	function get_opts ($return_assoc=false,$refresh=true) {
		$def = $this->get_default_opts();
		$opts = array_merge($def,get_option(ONETPOP_OPTIONS_FIELD, array()) );

		# Object conversion
		$opts_object = (object)array_merge($def,get_option(ONETPOP_OPTIONS_FIELD, array()) );
		$opts_object->modules = (object)array_merge($def['modules'], $opts_object->modules);

		# Update inner variable
		if ($refresh) $this->opts = $opts_object;
		//var_dump($opts,$opts_object);

		# Return in requested format
		return $return_assoc ? $opts : $opts_object;
	}

	/**
	* Update options field. Merging process: default < actual < new
	* In low level the settings stored as simple assoc array, stdClass will be converted.
	* @since 1.0
	* @param (array|object) new options
	* @return (boolean)
	**/
	function update_opts ($new_opts) {
		# Default arrays
		$merged = array();
		$def = $this->get_default_opts();
		$current = $this->get_opts(true);

		# Do the conversation for STD classes.
		if (is_object($new_opts)) $new_opts = (array)$new_opts;
		if (@is_object($new_opts['modules']) || @!is_array($new_opts['modules'])) $new_opts['modules'] = (array)$new_opts['modules'];

		# Merge first and seconds levels
		$merged = array_merge($def,$current,$new_opts);
		$merged['modules'] = array_merge($def['modules'],$current['modules'],$new_opts['modules']);

		# Save and refresh options
		update_option(ONETPOP_OPTIONS_FIELD, $merged);
		$this->get_opts();
	}



	/******************************************
	* WORDPRESS'S SO CALLED "HACKS"           *
	******************************************/


	/**
	* Register menu item for interface
	* @since 1.0
	* @param void
	* @return void
	**/
	function admin_menu() {
		$this->menu_item = add_options_page(__('Populify Pro','onetpop'),__('Populify Pro','onetpop'),'manage_options','onetpop-settings',array(&$this,'admin_ui_wrap') );
	}


	/**
	* Extend basic links in plugin admin, add 
	* @since 1.0
	* @param (array) current links
	* @return (array) updated links
	**/
	function plugin_settings_link ($links) {
		if (current_user_can( $this->cap)) array_push( $links, '<a href="options.php?page=onetpop-settings">'.__("Settings","onetpop").'</a>' );
		return $links;
	}

	/**
	*
	**/
	function admin_scripts($hook_suffix) {
	global $current_screen;
		wp_register_script("onetpop-modernizr", plugins_url( 'assets/js/modernizr.js', __FILE__ ), array(), "1.0",true);
		wp_register_script("onetpop-toggleswitch", plugins_url( 'assets/js/jquery-ui.toggleSwitch.min.js', __FILE__ ), array("jquery"), "1.0",true);
		wp_register_script("onetpop-main", plugins_url( 'assets/js/main.js', __FILE__ ), array("jquery"), "1.0",true);
		wp_register_style("onetpop-postmeta", plugins_url( 'assets/styles/postmeta.css', __FILE__));
		wp_register_style("onetpop-main", plugins_url( 'assets/styles/main.css', __FILE__));

		$lang = array(
			"lng" => array(
				"button" => array(
					"on" => __("on","onetpop"),
					"off" => __("off","onetpop"),
					"yes" => __("yes","onetpop"),
					"no" => __("no","onetpop")
					)
				),
			"prefix" => ONETPOP_OPT_PREFIX
			);
		wp_localize_script("onetpop-main", "onetpopMain", $lang );

		# Enqueue scipts for main admin UI
		if ( $hook_suffix == $this->menu_item ) {
			wp_enqueue_script("onetpop-modernizr");
			wp_enqueue_script("onetpop-toggleswitch");
			wp_enqueue_script("onetpop-main");
			wp_enqueue_style("onetpop-main");
		}
		# Register script for post editor
		else if ( $this->is_editor() ) {
			wp_enqueue_style("onetpop-postmeta");
		}
	}



	/******************************************
	* ADMIN INTERFACE                         *
	******************************************/



	/**
	* Draw admin UI. This function has many child functions and uses "views" folder where the views are stored.
	* @since 1.0
	* @param void
	* @return void
	**/
	function admin_ui_wrap () {
	global $wpdb,$wp_version;

		# Share settings
		$shareSettings = array(
			array(
				"var"=>"fb",
				"name"=>__("Facebook score","onetpop"),
				"hint"=>__("Facebook score will be refreshed each hour for each post.","onetpop")
			),
			array(
				"var"=>"tw",
				"name"=>__("Twitter score","onetpop"),
				"hint"=>__("Twitter score will be refreshed each hour for each post.","onetpop")
			),
			array(
				"var"=>"gp",
				"name"=>__("Google+ score","onetpop"),
				"hint"=>__("Google+ score will be refreshed each hour for each post.","onetpop")
			),
			array(
				"var"=>"pint",
				"name"=>__("Pinterest score","onetpop"),
				"hint"=>__("Pinterest score will be refreshed each hour for each post.","onetpop")
			)
		);

		
		# saving datas
		$msg = "";
		if (!empty($_POST) && isset($_POST[ONETPOP_OPT_PREFIX.'nonce']) && wp_verify_nonce($_POST[ONETPOP_OPT_PREFIX.'nonce'],$this->opts->custom_id)) {
			# Prepare new options valud
			$new_opts = $this->get_default_opts();

			# 1st level checkboxes (booleans)
			$checboxes = array("global","double_count_filter");
			foreach ($checboxes AS $checbox) $new_opts[$checbox] = isset($_POST[ONETPOP_OPT_PREFIX.$checbox]) ? 1 : 0;

			# Modules
			$modules = array("longread_count","coms","fb","tw","gp","pint");
			foreach ($checboxes AS $checbox) $new_opts['modules'][$checbox] = isset($_POST[ONETPOP_OPT_PREFIX.$checbox]) ? 1 : 0;
			
			# Interval fields
			$numInputs = array("longread_mins","max_content_age","db_lifetime","prevent_db_lifetime");
			foreach ($numInputs AS $numInput) $new_opts[$numInput] = intval($_POST[ONETPOP_OPT_PREFIX.$numInput]);

			# Prevention method
			$new_opts['prevent_method'] = $_POST[ONETPOP_OPT_PREFIX."prevent_method"] != "db" ? "cookie" : "db";

			# Deploy new settings
			$this->update_opts($new_opts);

			# Truncate database
			if (isset($_POST[ONETPOP_OPT_PREFIX."truncate"])) {
				$wpdb->query("TRUNCATE TABLE `".ONETPOP_DB_VIEW."`"); // Throw collected stats
				$wpdb->query("TRUNCATE TABLE `".ONETPOP_DB_PREVENT."`"); // Throw  preventions
				$msg .= __("Populify database truncated.","onetpop")."<br/>";
			}

			$msg .= __("Settings saved.","onetpop");
		}

		# Pass some variables
		$tableSize = $this->table_size(array(ONETPOP_DB_VIEW,ONETPOP_DB_PREVENT),1);
		$opts = &$this->opts;

		require_once ONETPOP_VIEWS."/admin.php";
	}

	/**
	* Add help tab for admin page
	* @since 1.0
	* @param called from Wordpress
	* @return (string)
	**/
	function admin_help($contextual_help, $screen_id, $screen) {
		if ($screen_id == $this->menu_item) {
			$screen->add_help_tab( array(
				'id'      => 'onetpop-welcome',
				'title'   => __('Welcome', 'onetpop'),
				'content' => "<h2>".__("Dear user","onetpop")."</h2>".__('Thank you for using this plugin, we hope you like ONet Populify Pro for Wordpress. If you are looking for troubleshoot, feature list, known issues, please click on "Read me" from the left menu. (Readme and support available only in english.)', 'onetpop')
				)
			);
			// Display readme from file (added line breaks)
			$screen->add_help_tab( array(
				'id'      => 'onetpop-readme',
				'title'   => __('Readme', 'onetpop'),
				'content' => nl2br(file_get_contents(dirname(__FILE__) . '/readme.txt' ))
				)
			);
		}
		return '';
	}



	/******************************************
	* POST EDITOR                             *
	******************************************/



	/**
	* Register metabox for post editor
	* @since 1.0
	* @param void
	* @param void
	**/
	function postmeta_wrap() {
		if ($this->opts->global != 1) return; // If plugin turned off completly, "hide" the metabox
		add_meta_box(
			'postmeta_dom',
			__( 'Post Popularity', 'onetpop'),
			array(&$this,'postmeta_inner'),
			'post',
			'side',
			'core'
		);
	}

	/**
	* Prepare required informations and echo the metabox content (used: views/postmeta.php)
	* @since 1.0
	* @param (object) Post meta
	* @return void
	**/
	function postmeta_inner ($post) {
	global $wpdb;
		# Load main global index
		$SQL = "SELECT SUM(point_index) AS `index`, SUM(view) AS `views`, SUM(longread) AS `longread` FROM ".ONETPOP_DB_VIEW." WHERE `ID`='".$post->ID."' GROUP BY `ID`";
		$idxs = $wpdb->get_row($SQL);
		
		# Fetch social points and stuff
		$count = new ONetPopulifyPro_counter_script($post->ID);
		$soc = $count->fetch_social();
		$coms = $count->comment_count(1,time());
		$opts = $this->opts;
		$isDismissed = get_post_meta($post->ID,ONETPOP_OPT_PREFIX."dismiss_counter",true);

		# Echo the output
		wp_nonce_field(plugin_basename( __FILE__ ), ONETPOP_OPT_PREFIX.'postmeta_nonce' ); // Nonce field
		require_once ONETPOP_VIEWS."/postmeta.php"; // Push all the stuff to the view
	}

	/**
	* Saving the metabox content
	* @since 1.0
	* @param (int) post ID
	* @return void
	**/
	function postmeta_save ($post_id) {
	global $post;

		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
		if ( !wp_verify_nonce( $_POST[ONETPOP_OPT_PREFIX.'postmeta_nonce'], plugin_basename(__FILE__)) ) return;
		if ( !current_user_can( 'edit_post', $post_id ) ) return;

		# Finaly, doing my stuff.
		$dismiss = isset($_POST[ONETPOP_OPT_PREFIX.'dismiss_counter']) ? 1 : 0;
		update_post_meta($post_id, ONETPOP_OPT_PREFIX."dismiss_counter", $dismiss);
	}



	/******************************************
	* CLIENT FUNCTIONS                        *
	******************************************/



	// Process the counting algorythm 
	// Detect current post and perform default calculations
	function oPPP_counter_client() {
		if (get_option(ONETPOP_OPT_PREFIX."global") == 0) return; // stop the counter if inactive
		if (is_single()) {
			$post = get_post();
			$storange = intval(get_option(ONETPOP_OPT_PREFIX."store_range"));

			if (get_post_meta($post->ID,"oppp_dismiss_counter",true) != 1 && ($storange == 0 || strtotime($post->post_modified) > (time() - ($storange*60*24*24))) ) {
				$counter = new ONetPopulifyPro_counter_script($post->ID);
				$counter->add_view();

				// Check category cache
				$cats = get_post_meta($post->ID,"oppp_categories",true);
				if (strlen($cats) < 100000) {
					$wp_cats = wp_get_post_categories($post->ID);
					update_post_meta($post->ID,"oppp_categories",";".implode(";",$wp_cats).";");
				}
			}
		}
	}


	// Javascript post process
	// Do not merge this function with oPPP_counter_client because user can use counter script without calling oPPP_counter_client
	function counter_post_process () {
		if (!is_single()) return; // Okey, this will prevent unwanted script calls. Cool.
	?>
		<script type="text/javascript">
			<?php  if (is_array($GLOBALS['ONETPOP_COOKIE_QUEUE'])) : ?>
			/* Add prevention cookie on client side */
			window.oppp_setCookie = function (name,val,expires) {
				var uts = Math.round((new Date()).getTime() / 1000); // Get current unix timestamp
				var exp = uts + parseInt(expires); // Calc expire UTS and make milisec ready
				document.cookie = name + "=" + escape(val) + (expires == null ? "" : "; expires=" + (new Date(exp*1000)).toGMTString() ); // Set cookie
			}
			/* Set all cookies from queue */
			window.opppCookieMeta = <?=json_encode($GLOBALS['ONETPOP_COOKIE_QUEUE'])?>;
			for (var i=0;i<window.opppCookieMeta.length;i++) {
				var drop = window.opppCookieMeta[i];
				oppp_setCookie(drop.name,drop.value,parseInt(drop.expire)*24*60*60);
			}
			<?php endif; ?>
			<?php if (get_option(ONETPOP_OPT_PREFIX."longread_count")) : ?>
			/* Set longread counter */
			function oppp_isVisible () {
				var d = document;
				if (d.hidden == true || d.mozHidden == true || d.msHidden == true || d.webkitHidden == true) return false;
				else return true; // backward compatbility
			}
			window.opppPostID = <?=intval(get_the_ID())?>; // current post id
			window.opppLongread = 0; // +1 point added each second when tab/window/article is on focus
			window.opppLongreadTimeout = <?=intval(get_option(ONETPOP_OPT_PREFIX."longread_mins"))?>*60;
			setInterval(function () {
				if (oppp_isVisible()) { window.opppLongread++; }
				if (window.opppLongread == window.opppLongreadTimeout) {
					jQuery.post("?oPPP_push_longread=1",{POST_ID:window.opppPostID}); // Solid push, no error handling.
				}
			},1000);
			<?php endif; ?>
		</script>
	<?php
	}


	// Okey, now here comes the "long read" part of the story
	function counter_longread($wp) {
	global $wpdb,$current_user;
		get_currentuserinfo();
		if (isset($_GET['oPPP_push_longread']) && isset($_POST['POST_ID'])) {
			$post = get_post($_POST['POST_ID']);
			$storange = intval(get_option(ONETPOP_OPT_PREFIX."store_range"));

			if (get_post_meta($post->ID,"oppp_dismiss_counter",true) != 1 && ($storange == 0 || strtotime($post->post_modified) > (time() - ($storange*60*24*24))) ) {

				$counter = new ONetPopulifyPro_counter_script(intval($_POST['POST_ID']));
				$counter->add_longread();
				
			}
			exit;
		}
	}


	/******************************************
	* UTILITY FUNCTIONS                       *
	******************************************/



	/**
	* Convert bytes into formated string
	* @param (int) bytes to format
	* @param [(int) number of decimals]
	* @return (string) formated size
	**/
	function size_format ($bytes, $decimals = 0 ) {
		$quant = array(
			'TB' => 1099511627776,	// pow( 1024, 4)
			'GB' => 1073741824,		// pow( 1024, 3)
			'MB' => 1048576,		// pow( 1024, 2)
			'kB' => 1024,			// pow( 1024, 1)
			'B' => 1,				// pow( 1024, 0)
		);

		// Find target
		foreach ( $quant as $unit => $mag ) {
			if ( doubleval($bytes) >= $mag ) {
				return number_format_i18n($bytes / $mag, $decimals).' ' .$unit;
			}
		}

		// If no match return with 0/false
		return __("Empty","onetpop");
	}

	/**
	* Get a table size and format
	* @param (string | array) table name(s)
	* @param [(boolean) output format]
	* @param [(boolean) empty table autocorrection]
	* @return (int | string) return bytes or formated size
	**/
	function table_size ($tables,$formated=true,$autocorr=true) {
	global $wpdb;
		$size = 0;
		if (!is_array($tables)) $tables = array($tables); // if input is not array

		// Get all table size
		foreach ($tables AS $tbl) {
			$ret = $wpdb->get_row("SHOW TABLE STATUS LIKE '".$tbl."'",ARRAY_A);
			if (!empty($ret['Data_length'])) {
				if ($autocorr && $ret['Rows'] < 1) { /* Catch "empty" table */ }
				else { $size += $ret['Data_length']; }
			}
		}

		// Never return negative value
		if ($size < 0) $size = 0;

		// Return result
		if ($formated == true) return $this->size_format($size);
		else return intval($size);
	}

	/**
	* Check if the current page is a post edit page
	* @since 1.0
	* @param  (string) "new" - new post page, "edit" - edit post page, "null" for either
	* @return boolean
	**/
	function is_editor ($new_edit=null) {
	global $pagenow;
		// Make sure we are on the backend
		if (!is_admin()) return false;

		if($new_edit == "edit") return in_array( $pagenow, array( 'post.php',  ) );
		// Check for new post page
		elseif($new_edit == "new") return in_array( $pagenow, array( 'post-new.php' ) );
		// Check for either new or edit
		else return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
	}
}




/*+++++++++++++++++++++++++++++++++++++++++++
Populify Widget
+++++++++++++++++++++++++++++++++++++++++++*/




class ONetPopulifyPro_Widget extends WP_Widget {
	function ONetPopulifyPro_Widget () {
		$widget_ops = array('classname' => 'ONetPopulifyPro_Widget', 'description' => __("Highly customizable popular post lister", "onetpop"));
		$this->WP_Widget('ONetPopulifyPro_Widget', __("Populify Pro", "onetpop"), $widget_ops);
	}

	function default_options () {
		return array(
			'title' 		=> __("Popular posts", "onetpop"),
			'daylimit'		=> 14,
			'postlimit'		=> 5,
			'noposttext'	=> __("Not enough data. Keep browsing.", "onetpop"),
			'catLimit'		=> array(0)
			);
	}

	function form($instance) {
		$instance = wp_parse_args((array) $instance, $this->default_options());
		$title = $instance['title'];
		$daylimit = $instance['daylimit'];
		$postlimit = $instance['postlimit'];
		$noposttext = $instance['noposttext'];
		$catLimit = array_flip($instance['catLimit']);

		$cat_settings = array(
			'type'				=> 'post',
			'hide_empty'		=> 0,
			'hierarchical'		=> true,
			'taxonomy'			=> 'category',
			'parent'			=> 0);
		$cats = get_categories($cat_settings);
		$cats_html = "<option value='0'".(isset($catLimit[0]) ? " selected='selected'" : "").">".__("-- All categories -- ", "onetpop")."</option>";
		foreach ($cats AS $cat) {
			$cats_html .= "<option value='".$cat->term_id."'".(isset($catLimit[$cat->term_id]) ? " selected='selected'" : "").">".$cat->name."</option>";
		}

		?>
		<p>
			<label for="<?=$this->get_field_id('title')?>"><strong><?=__("Custom title", "onetpop")?></strong></label>
			<input class="widefat" id="<?=$this->get_field_id('title')?>" name="<?=$this->get_field_name('title')?>" type="text" value="<?=attribute_escape($title)?>" />
		</p>
		<p>
			<label for="<?=$this->get_field_id('noposttext')?>"><strong><?=__("Text when there's no posts to display", "onetpop")?></strong></label>
			<input class="widefat" id="<?=$this->get_field_id('noposttext')?>" name="<?=$this->get_field_name('noposttext')?>" type="text" value="<?=attribute_escape($noposttext)?>" />
			<small><?=__("Keep blank if you want to hide the widget if no displayable posts.", "onetpop")?></small>
		</p>
		<p>
			<label for="<?=$this->get_field_id('daylimit')?>">
				<strong><?=__("Maximum age for listed posts", "onetpop")?></strong><br/>
				<input size="2" id="<?=$this->get_field_id('daylimit')?>" name="<?=$this->get_field_name('daylimit')?>" type="text" value="<?=attribute_escape($daylimit)?>" />
				<?=__(" days old", "onetpop")?><br />
			</label>
			<small><?=__("Type zero or keep blank if you don't want to filter the listed posts by date", "onetpop")?></small>
		</p>
		<p>
			<label for="<?=$this->get_field_id('postlimit')?>">
				<strong><?=__("Number of posts to display", "onetpop")?></strong>
				<input class="widefat" id="<?=$this->get_field_id('postlimit')?>" name="<?=$this->get_field_name('postlimit')?>" type="text" value="<?=attribute_escape($postlimit)?>" /><br />
			</label>
			<small><?=__("5 is ideal but you can remove limit with typing zeron in the field.", "onetpop")?></small>
		</p>

		<p>
			<label for="<?=$this->get_field_id('catLimit')?>">
				<strong><?=__("Category filter", "onetpop")?></strong>
				<select class="widefat" id="<?=$this->get_field_id('catLimit')?>" name="<?=$this->get_field_name('catLimit')?>[]" type="text" multiple="multiple" style="height:150px;"><?=$cats_html?></select><br />
			</label>
			<small><?=__("Hold CTRL/CMD key to select multiple items. Select \"All categories\" if you don't want category filter.", "onetpop")?></small>
		</p>
		<?php
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['daylimit'] = intval($new_instance['daylimit']);
		$instance['postlimit'] = intval($new_instance['postlimit']);
		$instance['catLimit'] = $new_instance['catLimit'];
		$instance['noposttext'] = $new_instance['noposttext'];
		if (!isset($new_instance['catLimit'][0]) || $new_instance['catLimit'][0] == 0) { $instance['catLimit'] = array(0=>0); }
		return $instance;
	}

	function widget($args, $instance) {
	global $wpdb;
		// Process datas
		extract($args, EXTR_SKIP);
		$title = !empty($instance['title']) ? $instance['title'] : __("Popular Posts","onetpop");
		// Create category filter
		$cat_filter = array();
		if ($instance['catLimit'][0] != 0) {
			foreach ($instance['catLimit'] AS $cat) {
				$cat_filter[] = "cats.meta_value LIKE '%;".$cat.";%'";
			}
		}
		$cat_filter = implode(" OR ", $cat_filter);

		// Time limit
		$time_filter = "";
		if ($instance['daylimit'] > 0) {
			$time_filter = " AND post.post_date>'".date("Y-m-d",time() - ($instance['daylimit']*24*60*60))." 23:59:59' ";
		}

		// Load stuffs
		$SQL = "SELECT rows.ID AS post_id, SUM(point_index) AS `index`, post.post_title AS title, dism.meta_value AS dism
			FROM `".ONETPOP_DB_VIEW."` AS rows 
			INNER JOIN ".$wpdb->prefix."posts AS post ON rows.ID=post.ID
			INNER JOIN ".$wpdb->prefix."postmeta AS cats ON rows.ID=cats.post_id
			INNER JOIN ".$wpdb->prefix."postmeta AS dism ON rows.ID=dism.post_id
			WHERE cats.meta_key='oppp_categories' AND post.post_status='publish' AND (dism.meta_key='oppp_dismiss_counter' AND dism.meta_value=0) ".$time_filter."  ".(strlen($cat_filter) > 0 ? " AND (".$cat_filter.")" : "")."
			GROUP BY rows.ID
			ORDER BY `index` DESC
			LIMIT 0,".$instance['postlimit'];
		$match = $wpdb->get_results($SQL,ARRAY_A);

		if (count($match) < 1 && strlen($instance['noposttext']) < 1) { return; }
		// Build the widget itself
		echo $before_widget;	
		echo $before_title . $title . $after_title;
		// Load all top posts
			if (count($match) < 1) {
				echo "<span class='oppp noEntry'>".$instance['noposttext']."</span>";
			} else {
				echo "<ul class='oppp lister'>";
				foreach ($match AS $drop) {
					echo "<li><a href='".get_permalink($drop['post_id'])."'>".$drop['title']."</a></li>";
				}
				echo "</ul>";
			}
		echo $after_widget;
	}
}






class ONetPopulifyPro_counter_script {
	private $opts = array();
	private $POST_ID; // Default post id
	private $innerTime; // Innder time (YYYYMMDD)
	private $time; // Unix time 
	private $prevent_time; // Prevention timeout - IN DAYS

	/**
	* Construct the inner struct
	* @since 1.0
	* @param (int) POST id
	* @param [(int) custom time]
	* @return (boolean) always true
	**/
	public function __construct ($POST_ID,$time=0) {
	global $ONetPopulifyPro_inst;
		# Load instance options from global namespace
		$this->opts = $ONetPopulifyPro_inst->get_opts();

		# Set default variables
		$this->set_POST_ID($POST_ID);
		$this->set_time($time > 0 ? $time : time());
		$this->prevent_time = (int)$this->opts->lifetime_prevent_db;
		if ($this->prevent_time < 1) $this->prevent_time = 365; // one year timeout if prevention set to "unlimited"
		
		# perform autoclan
		$this->autoClean(); 
	}

	/**
	* Check if provided custom ID is valid. If not use the default value. Even it the default empty return false
	* @since 1.0
	* @param (int) custom post ID
	* @return (int) current post ID
	**/
	private function get_id($custom=0) {
		if ($custom > 0) return intval($custom);
		else if ($this->POST_ID > 0) return $this->POST_ID;
		else return 0;
	}


	/**
	* Return with inner formated timestamp YYYYmmdd DAY based
	* @since 1.0
	* @param (int) unix time stamp
	* @return (string) formated date
	*/
	private function inner_stamp ($time=0) {
		if ($time == 0) $time = time();
		return date("Ymd",$time);
	}


	/**
	* Set post ID
	* @since 1.0
	* @param (int) new post id
	* @return (int) current post ID
	**/
	public function set_POST_ID ($NEW_ID) {
		$this->POST_ID = intval($NEW_ID);
		return $this->POST_ID;
	}


	/**
	* Set class time
	* @since 1.0
	* @param (int) unix timestamp
	* @return (int) unix timestamp
	**/
	public function set_time ($time=0) {
		$this->time = $time > 0 ? $time : time();
		$this->innerTime = $this->inner_stamp($this->time);
		return $this->time;
	}


	/**
	* Check day entry (if target entry does not exists create). Do not use for period data query
	* @since 1.0
	* @param [(int) unix timestamp]
	* @param [(boolean)  return with array of row or "0"]
	* @return (int) entry row ID || (array) entry if get_row is true
	**/
	private function check_day ($time=0,$get_row=false) {
	global $wpdb;

		$POST_ID = $this->get_id();
		$comment_time = $time > 0 ? $time : $this->time;
		$intime = $time;
		$time = $time > 0 ? $this->inner_stamp($time) : $this->innerTime;
		
		# Check for existing entry row and return its ID
		$chk = $wpdb->get_row("SELECT * FROM `".ONETPOP_DB_VIEW."` WHERE `ID`=".$POST_ID." AND `date`='".$time."'",ARRAY_A);
		if ($chk == 0) { } //return 0;
		else { return $get_row == true ? $chk : $chk['rid']; }

		# Fetch social score and comments
		$soc = $this->fetch_social($POST_ID);
		$coms = $this->comment_count($comment_time,$POST_ID);

		# Creating query and run it
		$SQL = "INSERT INTO `".ONETPOP_DB_VIEW."` (`ID`,`view`,`comment`,`fb`,`tw`,`gp`,`pint`,`soc_last_check`,`longread`,`point_index`,`date`)
				VALUE (".$POST_ID.",0,".(int)$coms.",".(int)$soc['fb'].",".(int)$soc['tw'].",".(int)$soc['gp'].",".(int)$soc['pint'].",".(int)$this->time.",0,0,'".$time."')";
		if (!$wpdb->query($SQL)) return 0; // If query fails return 0
		else return $get_row == true ? $this->check_day($intime,1) : $wpdb->insert_id;
	}


	/**
	* Check if user has prevention entry
	* @since 1.0
	* @param (string) prevention type (eg: view,longread)
	* @return (boolean) 1 if there is prevention entry 0 if no entry
	*/
	private function is_prevented ($type) {
	global $wpdb;
		$POST_ID = $this->get_id();

		if ( $this->opts->double_count_filter) {
			# if prevention method is Database check for prevention entry
			$SQL = "SELECT COUNT(*) FROM `".ONETPOP_DB_PREVENT."` WHERE `type`='".$type."' AND `time`>".($this->time - ($this->prevent_time*24*60*60))." AND `obj_id`=".$POST_ID." AND ip='".$_SERVER['REMOTE_ADDR']."'";
			if ( $this->opts->prevent_method == "db" && (int)$wpdb->get_var($SQL) > 0) {
				return true;
			# If prevention cookie exists
			} else if ( $this->opts->prevent_method == "cookie" && isset($_COOKIE[ONETPOP_PREVENT_COOKIE.$type.$POST_ID])) {
				return true;
			}
		}
		return false;
	}


	/**
	* Add prevention entry
	* @since 1.0
	* @param (string) prevention type (eg: view,longread)
	* @return (boolean) always true
	**/
	private function add_prevention ($type) {
	global $wpdb;
		$POST_ID = $this->get_id();
		# global cookie val does not exists
		if (!isset($GLOBALS['ONETPOP_COOKIE_QUEUE']) || !is_array($GLOBALS['ONETPOP_COOKIE_QUEUE'])) $GLOBALS['ONETPOP_COOKIE_QUEUE'] = array();

		if ( $this->opts->double_count_filter == 1) {
			# Check if prevention set to cookie
			if ( $this->opts->prevent_method == "cookie") {
				$cookie = ONETPOP_PREVENT_COOKIE.$type.$POST_ID;
				$expires = $this->prevent_time; // IN DAYS,need conversion before set
				if (!headers_sent()) { setcookie($cookie,1,($this->time + ($expires*24*60*60)),"/"); } // Set cookie in the regular way if headers not sent yet
				else { $GLOBALS['ONETPOP_COOKIE_QUEUE'][] = array("name" => $cookie, "value" => 0, "expire" => $expires); } // Otherwise ask client side (JS) to set cookie
			# Add prevention entry to database
			} else if ( $this->opts->prevent_method == "db") {
				$wpdb->query("INSERT INTO `".ONETPOP_DB_PREVENT."` (`type`,`obj_id`,`time`,`ip`) VALUE ('".$type."',".$POST_ID.",".$this->time.",'".$_SERVER['REMOTE_ADDR']."') ");
			}
		}
		return true;
	}


	/**
	* Fetch share counter from external sources
	* @since 1.0
	* @param: void
	* @return (array) {"fb" => (int) Facebook shares, "tw" => (int) Twitter shares}
	**/
	public function fetch_social () {
		$POST_ID = $this->get_id();

		$perm = get_permalink($POST_ID); // Get permalink to check social value
		$ret = array("fb"=>0,"tw"=>0,"gp"=>0,"pint"=>0); // Set default variables
		# Grab data from FB
		if ( $this->opts->modules->fb ) {
			$fetched_raw = wp_remote_get("http://graph.facebook.com/?id=".urlencode($perm));
			if (!is_wp_error($fetched_raw)) {
				$fetched = json_decode($fetched_raw['body'],1);
				$ret['fb'] = (int)$fetched['shares'];
			}
		}
		# Grab data from TW
		if ( $this->opts->modules->tw ) {
			$fetched_raw = wp_remote_get("http://urls.api.twitter.com/1/urls/count.json?url=".urlencode($perm));
			if (!is_wp_error($fetched_raw)) {
				$fetched = json_decode($fetched_raw['body'],1);
				$ret['tw'] = (int)$fetched['count'];
			}
		}
		# Grab data from google plus
		if ( $this->opts->modules->gp ) {
			$fetched_raw = wp_remote_get("https://plusone.google.com/_/+1/fastbutton?url=".urlencode($perm));
			if (!is_wp_error($fetched_raw)) {
				$fetched = json_decode($fetched_raw['body'],1);
				
				preg_match( '/window\.__SSR = {c: ([\d]+)/', $fetched, $matches );
				if( isset( $matches[0] ) ) $ret['gp'] = (int)str_replace( 'window.__SSR = {c: ', '', $matches[0] );
				$ret['gp'] = 0;
			}
		}
		# Grab data from Pinterest
		if ( $this->opts->modules->pint ) {
			$fetched_raw = wp_remote_get("http://api.pinterest.com/v1/urls/count.json?callback=&url=".urlencode($perm));
			if (!is_wp_error($fetched_raw)) {
				$fetched = json_decode($fetched_raw['body'],1);
				$ret['pint'] = (int)$fetched['count'];
			}
		}

		return $ret;
	}


	/**
	* Resync social shares on target post/day/record
	* @since 1.0
	* @param void
	* @return (array) fb and tw score for the record
	**/
	public function social_resync () {
	global $wpdb;
		$POST_ID = $this->get_id();
		$time = $this->time;
		$record = $this->check_day($time,true);

		// Refresh social informations if cache is not older than one hour. Social resync works only for "today" record
		if ($record['soc_last_check'] < ($time-(60*60)) && $record['soc_last_check'] > ($time-(24*60*60))) {
			$soc = $this->fetch_social(); // Grab the latest social stats
			$wpdb->query("UPDATE `".ONETPOP_DB_VIEW."` SET `fb`='".$soc['fb']."',`tw`='".$soc['tw']."',`gp`='".$soc['gp']."',`pint`='".$soc['pint']."',`soc_last_check`='".$this->time."' WHERE `rid`='".$record."'"); // Update
			return $soc; // And return with FB/TW array
		} else {
			return array("tw" => $record['tw'], "fb" => $record['fb'], "gp" => $record['gp'], "pint" => $record['pint']);
		}
	}


	/**
	* Get comments by day 
	* @since 1.0
	* @param [(int) start time]
	* @param [(int) final time]
	* @return (int) number of comments ofr time of period
	**/
	public function comment_count ($time=0,$final_time=0) {
	global $wpdb;
		$POST_ID = $this->get_id();
		if ($time == 0) $time = $this->time;
		$final_time = $final_time > 0 ? intval($final_time) : $time;

		$SQL = "SELECT Count(*) FROM ".$wpdb->prefix."comments WHERE comment_approved=1 AND comment_post_ID=".$POST_ID." AND (comment_date>'".date("Y-m-d",$time)." 00:00:00' AND comment_date<'".date("Y-m-d",$final_time)." 23:59:59')";
		return $wpdb->get_var($SQL);
	}


	/**
	* Resync comments on post/day
	* @since 1.0
	* @param [(int) unix time]
	* @param [(int) record ID] 
	* @return (int)
	**/
	public function comment_resync ($time=0,$rid=0) {
	global $wpdb;
		$POST_ID = $this->get_id();
		if ($time == 0) $time = $this->time;
		$record = $rid < 1 ? $this->check_day($time) : intval($rid); // load record ID by time if not provided
		
		$comments = $this->comment_count($time);
		$wpdb->query("UPDATE `".ONETPOP_DB_VIEW."` SET `comment`='".$comments."' WHERE `rid`='".$record."'");
		return $comments;
	}


	/**
	* Add extra points to a post
	* @since 1.0
	* @param (string) count type
	* @return (boolean) status
	**/
	private function add_point ($type) {
	global $wpdb;
		$POST_ID = $this->get_id();
		if (!$this->is_prevented($type)) {
			$record_id = $this->check_day();
			$SQL = "UPDATE `".ONETPOP_DB_VIEW."` SET `".$type."`=`".$type."`+1 WHERE `rid`=".$record_id;
			if (!$wpdb->query($SQL)) return false;
			else $this->add_prevention($type);
			$this->index_recalc();
		}
		
		return true;
	}

	/**
	* Add view count NOTE: Aviable only for valid posts
	* @since 1.0
	* @param: void
	* @return (boolean) status
	**/
	public function add_view () { return $this->add_point("view"); }


	/** 
	* Add Extra point for longread
	* @since 1.0
	* @param void
	* @return (boolean) status
	**/
	public function add_longread () { return $this->add_point("longread"); }

	/**
	* Recalculating the main index value by precached values (basedon global time and post id OR specified row id). DOES NOT CREATE ENTRY IF NO MATCH. 
	* @since 1.0
	* @param: [(int) forced record id]
	* @return: (int) new index or 0 (if no match)
	**/
	public function index_recalc ($rid=0) {
	global $wpdb;
		$POST_ID = $this->get_id();
		$time = $this->time;
		$rid = intval($rid);

		# Resync social and comment counters
		$this->social_resync();
		$this->comment_resync();
			
		$match = $wpdb->get_row("SELECT * FROM ".ONETPOP_DB_VIEW." WHERE (`rid`='".$rid."' AND rid>0) OR (`ID`='".$POST_ID."' AND `date`='".$this->innerTime."')",ARRAY_A);
		if ($match != null) {
			# view, comment, long_read
			$idx = 0;
			$idx += $match['view'];
			$idx += $match['comment']*3;
			$idx += $match['longread']*2;
			# fb, tw - load previous day to get differences
			$yday = $this->check_day($time-(24*60*60),true);
			if ($yday != 0) {
				$idx += ($yday['fb'] - $match['fb'])*5;
				$idx += ($yday['tw'] - $match['tw'])*5;
				$idx += ($yday['gp'] - $match['gp'])*5;
				$idx += ($yday['pint'] - $match['pint'])*5;
			}
			$wpdb->query("UPDATE ".ONETPOP_DB_VIEW." SET point_index='".$idx."' WHERE rid='".$match['rid']."'");
		} else return 0;
	}

	/**
	* Remove all outdated items from database too keep the. NOTE: No call limits. Time/call limit must be applied in the context.
	* @since 1.0
	* @param: void
	* @return: (boolean) always true
	**/
	private function autoClean () {
	global $wpdb,$ONetPopulifyPro_inst;
		$wpdb->query("DELETE FROM `".ONETPOP_DB_PREVENT."` WHERE `time`<".($this->time-($this->prevent_time*24*60*60))); // Remove old prevention entries
		if ( !$this->opts->db_lifetime_db) {
			$time = $this->time-(60*60*24*(int)$this->opts->db_lifetime_db ); // Get last aviable timestamp
			$wpdb->query( "DELETE FROM `".ONETPOP_DB_VIEW."` WHERE `date`<".date("Ymd",$time) );
		}
		# Update last autoclean in database
		$ONetPopulifyPro_inst->update_opts(array("last_autoclean"=>$this->time));
		return true;
	}
}
?>