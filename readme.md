# ONet Populify Pro

DESC COMES HERE

## Credits

[József Koller](http://profiles.wordpress.org/orosznyet/) - Main codebase

**[Donate link](http://onetdev.com/repo/onet-populify-pro)**

## Download

You can download the Wordpress plugin just click on the download link. **[Download here](http://wordpress.org/plugins/onet-populify-pro/)**

## Installation

1. Install *ONet Populify Pro* either via the WordPress.org plugin directory, or by uploading the files to your server (download link above).
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Chose a method listed in description.

## FAQ

You can read the most recent FAQ list on the wordpress plugin page also if you are looking for support you can do that on the Wordpress plugin poge too.

## Changelog

### 1.0
* Initial release