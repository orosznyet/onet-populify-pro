=== ONet Populify Pro ===
Contributors: orosznyet
Donate link: http://onetdev.com/repo/onet-populify-pro
Tags: facebook, googleplus, twitter, pinterest, point, social, advanced
Requires at least: 3.5
Tested up to: 3.8
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Get social value for your posts based on Twitter, Facebook, Pinterest, Google+ share counters, average reading time and comment number. Usefull for advanced users.

== Description ==

DESC COMES HERE

== Installation ==

1. Install ONet Populify Pro either via the WordPress.org plugin directory, or by uploading the files to your server
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Chose a method listed in description.

== Frequently Asked Questions ==

FAW COMES HERE

== Screenshots ==

1. /assets/screenshot-1.png
2. /assets/screenshot-2.png
3. /assets/screenshot-3.png

== Changelog ==

= 1.0 =
* Initial release

== Upgrade Notice ==

= 1.0 =
Initial release