var $ = jQuery;

window.onetAdminCheckForm = function () {
	var s = onetpopMain.prefix, $ = jQuery;
	// Start with global chec
	if ($("input#"+s+"global").is(":checked")) { $(".hideIfNoGlobal").show(); }
	else { $(".hideIfNoGlobal").hide(); }

	// Double count prevention
	if ($("input#"+s+"double_count_filter").is(":checked") && $("input#"+s+"global").is(":checked")) { $(".hideIfNoPrevention").show(); }
	else { $(".hideIfNoPrevention").hide(); }
	
	// Longread minute counter
	/*if ($("input#"+s+"longread_count").is(":checked")) { $(".hideIfNoLongread").show(); }
	else { $(".hideIfNoLongread").hide(); }*/
}

$(document).ready(function () {
	// VERTICALLY ALIGN FUNCTION
	$.fn.vAlign = function() {
		return this.each(function(i){
			var ah = $(this).height();
			var ph = $(this).parent().height();
			var mh = (ph - ah) / 2;
			$(this).css('margin-top', mh);
		});
	};

	// Vertical align left side
	$(".onet-admin-panel .row .left").vAlign();

	$(".parse-onet-onof").each(function () {
		// Button texts
		if ($(this).hasClass("onet-yesno")) {
			var buttons = {
				btn1 : onetpopMain.lng.button.yes,
				btn2 : onetpopMain.lng.button.no
			};
		} else {
			var buttons = {
				btn1 : onetpopMain.lng.button.on,
				btn2 : onetpopMain.lng.button.off
			};
		}

		// Add custom UI
		$('<input type="button" class="onet-button on a200 '+$(this).prop("id")+'" id="'+$(this).prop("id")+'_on" value="'+buttons.btn1+'" />'+
			'<input type="button" class="onet-button off a200 '+$(this).prop("id")+'" id="'+$(this).prop("id")+'_off" value="'+buttons.btn2+'" />').
		insertBefore($(this));
		// Hide checkbox, comment for debug
		$(this).hide();

		// Add checkbox wathcer
		$(this).change(function () {
			$(".onet-button." + $(this).prop("id")).removeClass("active");
			$("#" + $(this).prop("id") + "_" + ($(this).is(":checked") ? "on" : "off")).addClass("active");
			// Refresh UI on every change.
			window.onetAdminCheckForm();
		});
		// Trigger watcher to refresh custom UI
		$(this).change();
	});

	// Add custom switch ui handlers
	$(".onet-button.on, .onet-button.off").click(function () {
		var item = $("#" + $(this).prop("id").replace(/_on$|_off$/gi,''));
		if ($(this).hasClass("on")) item.prop({checked:"checked"});
		else  item.removeAttr("checked");
		item.change();
		delete(item);
	});

	// Label correction for input name instead of id
	$("label.onet-radioName").click(function (e)  {
		var items = $("input[name='"+$(this).attr("for")+"']");
		if (items.length < 1) return;

		// Selected radio button search
		for (var i=0;i<items.length;i++) {
			// Find checked radiobutton
			if (items[i].checked == true) {
				if ((i+1) == items.length) $(items[0]).click();
				else  $(items[i+1]).click();
				i = items.length+1;
			}
		}

		// If there is no selected
		if (i < items.length) $(items[0]).click();
	});

	// Refresh UI
	window.onetAdminCheckForm();
});